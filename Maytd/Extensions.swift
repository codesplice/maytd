//
//  Extensions.swift
//  Maytd
//
//  Created by Sam Ritchie on 28/9/16.
//  Copyright © 2016 codesplice pty ltd. All rights reserved.
//

import UIKit
import CoreLocation
import ZLSwipeableViewSwift

extension MutableCollection where Indices.Iterator.Element == Index {
    /// Shuffles the contents of this collection.
    mutating func shuffle() {
        let c = count
        guard c > 1 else { return }
        
        for (unshuffledCount, firstUnshuffled) in zip(stride(from: c, to: 1, by: -1), indices) {
            let d: IndexDistance = numericCast(arc4random_uniform(numericCast(unshuffledCount)))
            guard d != 0 else { continue }
            let i = index(firstUnshuffled, offsetBy: d)
            swap(&self[firstUnshuffled], &self[i])
        }
    }
}

extension Sequence {
    /// Returns an array with the contents of this sequence, shuffled.
    func shuffled() -> [Iterator.Element] {
        var result = Array(self)
        result.shuffle()
        return result
    }
}


extension Array {
    func randomSelection() -> [Element] {
        var ret = [Element]()
        for i in self {
            if arc4random() % 2 == 0 { ret.append(i) }
        }
        return ret.shuffled()
    }
}

extension ZLSwipeableView {
    func updateIfNeeded() {
        if self.activeViews().isEmpty {
            let f = self.nextView
            self.nextView = f
        }
    }
}

extension CGRect {
    func centredIn(parent: CGRect) -> CGRect {
        return self.offsetBy(dx: parent.size.width / 2 - size.width / 2, dy: parent.size.height / 2 - size.height / 2)
    }
}
