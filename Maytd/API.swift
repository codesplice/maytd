//
//  API.swift
//  Maytd
//
//  Created by Sam Ritchie on 28/9/16.
//  Copyright © 2016 codesplice pty ltd. All rights reserved.
//

import UIKit
import ReactiveSwift
import CoreLocation
import Result

class API {
    
    static var testUsers = [
        User(id: 1, name: "Reginald", photo: #imageLiteral(resourceName: "nerd-m-1"), bio: "I like long walks to D5"),
        User(id: 2, name: "Clark", photo: #imageLiteral(resourceName: "nerd-m-2"), bio: "Let me show you my set"),
        User(id: 3, name: "Wesley", photo: #imageLiteral(resourceName: "nerd-m-3"), bio: "Be my grandmaster!"),
        User(id: 4, name: "Darlene", photo: #imageLiteral(resourceName: "nerd-f-1"), bio: "What opening do you play?"),
        User(id: 5, name: "Felicity", photo: #imageLiteral(resourceName: "nerd-f-2"), bio: "Doesn’t matter if you’re black or white"),
        User(id: 6, name: "Winifred", photo: #imageLiteral(resourceName: "nerd-f-3"), bio: "Wait until you see my endgame")
    ]
    
    func getNearbyUsers() -> SignalProducer<[User], NoError> {
        print("Checking the server for any spunky chess players nearby")
        return SignalProducer(value: API.testUsers.randomSelection()).delay(1, on: QueueScheduler.main)
    }
}

let api = API()

