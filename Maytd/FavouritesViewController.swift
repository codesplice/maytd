//
//  FavouritesViewController.swift
//  Maytd
//
//  Created by Sam Ritchie on 29/9/16.
//  Copyright © 2016 codesplice pty ltd. All rights reserved.
//

import UIKit

class FavouritesViewController: UITableViewController {
    
    var favouriteUsers = [User]()
    
    override func viewDidLoad() {

    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favouriteUsers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let user = favouriteUsers[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteCell") as! FavouriteCell
        cell.render(user: user)
        return cell
    }
}

class FavouriteCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var photoImageView: UIImageView!
    
    override func awakeFromNib() {
        photoImageView.layer.cornerRadius = 25
    }
    
    func render(user: User) {
        photoImageView.image = user.photo
        nameLabel.text = user.name
    }
}
