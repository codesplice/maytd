//
//  AppDelegate.swift
//  Maytd
//
//  Created by Sam Ritchie on 26/9/16.
//  Copyright © 2016 codesplice pty ltd. All rights reserved.
//

import UIKit
import CoreLocation
import ReactiveSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        return true
    }
}

