//
//  SwipeViewController.swift
//  Maytd
//
//  Created by Sam Ritchie on 28/9/16.
//  Copyright © 2016 codesplice pty ltd. All rights reserved.
//

import UIKit
import ZLSwipeableViewSwift

class SwipeViewController: UIViewController {
    @IBOutlet var profilePhotoImageView: UIImageView!
    @IBOutlet var profileNameLabel: UILabel!
    @IBOutlet var swipeView: ZLSwipeableView!
    
    override func viewDidLoad() {
        profilePhotoImageView.layer.cornerRadius = 25
        
        view.layoutIfNeeded()
        swipeView.allowedDirection = [.Left, .Right]
        swipeView.onlySwipeTopCard = true
        swipeView.numberOfActiveView = 6
        swipeView.numberOfHistoryItem = 0
        
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}


class UserCardView: UIView {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var bioLabel: UILabel!
    @IBOutlet var photoImageView: UIImageView!
    var id: Int!
    
    override func awakeFromNib() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.25
        layer.shadowOffset = CGSize(width: 0, height: 1.5)
        layer.shadowRadius = 4.0
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        layer.cornerRadius = 10
        clipsToBounds = true
    }
    
    func render(user: User) {
        id = user.id
        nameLabel.text = user.name
        bioLabel.text = user.bio
        photoImageView.image = user.photo
    }
}

