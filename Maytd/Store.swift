//
//  Store.swift
//  Maytd
//
//  Created by Sam Ritchie on 26/9/16.
//  Copyright © 2016 codesplice pty ltd. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result
import CoreLocation

struct User {
    var id: Int
    var name: String
    var photo: UIImage?
    var bio: String?
}





// MARK: Equatable
extension User: Equatable {}
func ==(lhs: User, rhs: User) -> Bool {
    return lhs.name == rhs.name
        && lhs.photo == rhs.photo
        && lhs.bio == rhs.bio
}
