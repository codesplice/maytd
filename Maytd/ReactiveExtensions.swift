//
//  ReactiveExtensions.swift
//  Maytd
//
//  Created by Sam Ritchie on 26/9/16.
//  Copyright © 2016 codesplice pty ltd. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift
import Result
import MapKit
import CoreLocation
import ZLSwipeableViewSwift

extension UIButton {
    var rac_tap: Signal<(), NoError> {
         return self.trigger(for: .touchUpInside).map { _ in () }
    }
}

extension ZLSwipeableView {
    var rac_swipe: SignalProducer<(UIView, Direction), NoError> {
        return SignalProducer<(UIView, Direction), NoError> { sink, disp in
            self.didSwipe = { v, d, _ in
                sink.send(value: (v, d))
            }
        }
    }
    
}
